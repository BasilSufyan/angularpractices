import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';


import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MakeTransferComponent } from './make-transfer/make-transfer.component';
import { RecentTransferComponent } from './recent-transfer/recent-transfer.component';
import { SearchFilterComponent } from './search-filter/search-filter.component';
import { TransactionRowComponent } from './transaction-row/transaction-row.component';
import { TransactionServiceService} from './transaction-service.service';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MakeTransferComponent,
    RecentTransferComponent,
    SearchFilterComponent,
    TransactionRowComponent
  ],
  imports: [
    BrowserModule,HttpModule
  ],
  providers: [TransactionServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
