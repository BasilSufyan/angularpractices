import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {Observable} from "rxjs";
import { Transactions } from './models/transactions';
import {map} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class TransactionServiceService {

  constructor(private http:Http) { }

  getTransactions(): Observable<Transactions[]>{
    return this.http.get('../assets/transactions.json')
           .pipe(map(res=> res.json().data));
    
    
  }
}
