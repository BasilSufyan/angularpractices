import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import {filters} from '../transaction-row/transaction-row.component';

@Component({
  selector: 'app-search-filter',
  templateUrl: './search-filter.component.html',
  styleUrls: ['./search-filter.component.css']
})
export class SearchFilterComponent implements OnInit {
  
  @ViewChild('amount') amount;
  @ViewChild('beneficiaries') beneficiaries;
  @ViewChild('date') date;

  public sortByDate = true;
  public sortByBeneficiaries = false;
  public sortByAmount = false;

  @Output() keyupevent:EventEmitter<any> = new EventEmitter();
  @Output() filterevent:EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onKeyUp(val){    
    setTimeout(() => {
      this.keyupevent.emit(val);
    }, 500);
    
  }
  filter(val){
      this.handleSort(val);
      this.filterevent.emit(val);
  }
  
  removeClass() {
    this.amount.nativeElement.className = '';
    this.date.nativeElement.className = '';
    this.beneficiaries.nativeElement.className = '';
  }

  handleSort(type) {
    this.removeClass();
    switch (type) {
      case filters.date:
        this.sortByDate = !this.sortByDate;
        this.date.nativeElement.className = (this.sortByDate ? 'up' : 'down') + ' active';
        break;

      case filters.beneficiaries:
        this.sortByBeneficiaries = !this.sortByBeneficiaries;
        this.beneficiaries.nativeElement.className = (this.sortByBeneficiaries ? 'up' : 'down') + ' active';
        break;

      case filters.amount:
        this.sortByAmount = !this.sortByAmount;
        this.amount.nativeElement.className = (this.sortByAmount ? 'up' : 'down') + ' active';
        break;
    }
  }

}
