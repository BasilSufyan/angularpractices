import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentTransferComponent } from './recent-transfer.component';

describe('RecentTransferComponent', () => {
  let component: RecentTransferComponent;
  let fixture: ComponentFixture<RecentTransferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecentTransferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentTransferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
