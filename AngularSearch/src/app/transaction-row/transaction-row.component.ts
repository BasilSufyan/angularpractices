import { Component, OnInit } from '@angular/core';
import { Transactions } from '../models/transactions';
import {TransactionServiceService} from '../transaction-service.service';
import { filter } from '../../../node_modules/rxjs/operators';
export enum filters {
  date = 1 ,
  beneficiaries = 2,
  amount = 3
}

@Component({
  selector: 'app-transaction-row',
  templateUrl: './transaction-row.component.html',
  styleUrls: ['./transaction-row.component.css']
})
export class TransactionRowComponent implements OnInit {

  public transactions:Transactions[];  
  private transactionsBak;
  public sortByDate = true;
  public sortByBeneficiary = false;
  public sortByAmount = false;

  constructor(private transactionSvc:TransactionServiceService) { }  

  ngOnInit() {
    this.transactionSvc.getTransactions().subscribe(transactions => {this.transactions = this.transactionsBak = transactions, console.log(this.transactions)});    
  }

  handleKeyUp(val){
    this.transactions = this.transactionsBak;
    if (val.length === 0) {
      return;
    }
    this.transactions = this.transactions.filter(obj => obj.merchant.toLowerCase().search(val.toLowerCase())!=-1);    
    console.log("Logged from transactions row",val);
    } 

    handleSort(val){      
      switch(val){
        case filters.date:
        this.handleDateSort();
        break;
        case filters.beneficiaries:
        this.handleBeneficiarySort();
        break;
        case filters.amount:
        this.handleAmountSort();
        break;
        
      }

    }

    handleDateSort(){
      this.sortByDate = !this.sortByDate;
      console.log(this.sortByDate);
      this.transactions = this.transactions.sort((a:Transactions,b:Transactions)=>{
       if(this.sortByDate){
        return a.transactionDate - b.transactionDate;
       } 
       else{
        return b.transactionDate - a.transactionDate;
       }
      });
    }

    handleBeneficiarySort(){
      this.sortByBeneficiary = !this.sortByBeneficiary;
      console.log(this.sortByBeneficiary);
      this.transactions = this.transactions.sort((a:Transactions,b:Transactions)=>{
       if(this.sortByBeneficiary){
        return a.merchant.localeCompare(b.merchant);       
       } 
       else{
        return b.merchant.localeCompare(a.merchant);
      }
      });
    }

    handleAmountSort(){
      this.sortByAmount = !this.sortByAmount;
      console.log(this.sortByAmount);
      this.transactions = this.transactions.sort((a:Transactions,b:Transactions)=>{
       if(this.sortByAmount){
        return a.amount.localeCompare(b.amount); 
       } 
       else{
        return b.amount.localeCompare(a.amount); 
       }
      });
    }
}
